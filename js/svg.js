(function ($) {
  Drupal.behaviors.svgmap = {
    attach: function (context, settings) {
      var regions = Drupal.settings.svgmap;
      var xCord, yCord;
      var $inforamtion = $('<div/>').addClass('information');
      var $close = $('<span />').addClass('close');
      var $text = $('<div/>').addClass('info-content');
      var windowWidth = $(window).width();
      var state = false;
      $inforamtion.append($close);
      $inforamtion.append($text);
      $(document).mousemove(function (e) {
        xCord = e.pageX;
        yCord = e.pageY;

      });
      /*set coord for popup*/
      function setCoords(object, x, y, Width) {
        if (1180 - x < popupWudth) {
          x = x - (popupWudth + 40);
        }
        object.css({'top': y - 50, 'left': x});
      }
      $('#wrapper').append($inforamtion);
      var popupWudth = $('.information').width();
      $inforamtion.hide();
      $('.close').bind('click', function () {
        $('.information').hide();
        state = false;
        console.log(state);

      });
      for (var city in regions) {
        /*Css fot active regions*/
        $('#RU-' + city).css({'fill': '#006768', 'stroke': '#00cdd2', 'cursor': 'pointer'});
        $('#RU-' + city).hover(function () {
          $(this).css({'fill': '#00cdd2', 'stroke': '#00cdd2'});
        }, function () {
          $(this).css({'fill': '#006768', 'stroke': '#00cdd2'});
        });
        /*bind click to active elements*/
        $('#RU-' + city).bind('click', function () {
          setCoords($inforamtion, xCord, yCord, windowWidth);
          $id = $(this).attr('id').split('-')[1];
          var content = "";
          regions[$id].forEach(function (item, i, arr) {
            content += '<span class="zavod-title">' + item.title + ' : </span>' + '<span class="zavod-count"> Количество заводов : ' + item.count + "</span><br>";
          });
          $text.html(content);
          console.log(state);
          if (!state) {
            $inforamtion.show();
            state = true;
          }

        });



      }

    }
  }
})(jQuery)